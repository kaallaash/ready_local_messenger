﻿using System;

namespace LocalMesseger
{
    class Encryption
    {
        public const int Key = 10;
        
        public static string Encrypt(string text, int key)
        {
            string newText = "";
            string space = " ";
            int number;

            for (int i = 0; i < text.Length; i++)
            {
                number = EncryptOneChar(text[i], key);

                if (i != text.Length - 1)
                {
                    newText += number + space;
                }
                else
                {
                    newText += number;
                }
            }

            return newText;
        }

        public static string Decrypt(string text, int key)
        {
            if (text == "")
            {
                return text;
            }

            var textArr = text.Split(' ');
            var decryptText = "";

            for (int i = 0; i < textArr.Length; i++)
            {
                decryptText += DecryptOneChar(Convert.ToInt32(textArr[i]), key);
            }

            return decryptText;
        }

        private static int EncryptOneChar(char ch, int key)
        {
            int number = Convert.ToInt32(ch) + key;
            return number;
        }

        private static string DecryptOneChar(int number, int key)
        {
            return Convert.ToString(Convert.ToChar(number - key));
        }
    }
}
