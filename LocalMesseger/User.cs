﻿using System;
using System.Collections;
using System.IO;

namespace LocalMesseger
{
    public class User
    {
        public string name;
        public int id;
        public string login;
        private string password;
        public ArrayList friendsId;
        public ArrayList friendsIdRequest;

        public void LoadData(string login)
        {
            string text = FileOperation.LoadTextInformation(Path.GetPathUserDataFile(login), Encryption.Key);
            string[] separator = new string[] { "\n" };
            string[] data = text.Split(separator, StringSplitOptions.None);

            name = data[0];
            id = Convert.ToInt32(data[1]);
            this.login = data[2];
            password = data[3];
            friendsId = GetFriensIdOrFriendsIdRequest(Path.GetPathUserFriendsFile(login));
            friendsIdRequest = GetFriensIdOrFriendsIdRequest(Path.GetPathUserFriendsRequestFile(login));
        }

        public void CreateData(string name, int id, string login, string password)
        {
            FileOperation.CreateUserFolder(login);
            FileOperation.CreateUserDataFile(name, id, login, password);
            LoadData(login);
        }

        public bool CheckPassword(string pass)
        {
            var check = password == pass ? true : false;
            return check;
        }

        private static ArrayList GetFriensIdOrFriendsIdRequest(string path)
        {
            ArrayList friensIdOrRequestFriendsId = new ArrayList();

            if (File.Exists(path))
            {
                string textFromFile = FileOperation.LoadTextInformation(path, Encryption.Key);
                string[] textArray = textFromFile.Split('\n');

                for (int i = 0; i < textArray.Length; i++)
                {
                    if (textArray[i] == "")
                    {
                        continue;
                    }

                    friensIdOrRequestFriendsId.Add(textArray[i]);
                }
            }

            return friensIdOrRequestFriendsId;
        }
    }
}