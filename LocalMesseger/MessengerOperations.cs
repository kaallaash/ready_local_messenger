﻿using System;
using System.IO;

namespace LocalMesseger
{
    public class MessengerOperations
    {
        public static void StartChat(User user, IMessage message)
        {
            int id = ChooseChatWithId(user, message);

            StartChatWithUser(user, id, message);
        }

        public static void CheckIdRequestFriend(User user, IMessage message)
        {
            if (UserOperation.CheckHaveRequestFriend(user))
            {
                message.ChooseShowRequestOrContinue(user);

                for(; ; )
                {
                    string choosing = Console.ReadLine();

                    if (choosing == "1")
                    {
                        message.ShowListFriendsRequest(user);
                        int idFriend = EnterRequestFriendId(user, message);
                        AcceptOrDeleteFriend(user, idFriend, message);
                        break;
                    }
                    else if (choosing == "2")
                    {  
                        break;
                    }
                    else
                    {
                        message.RepeatChooseOneOrTwo();
                    }
                }
            }
        }

        private static int ChooseChatWithId(User user, IMessage message)
        {
            int choosingID;
            message.ChooseChat();

            for(; ; )
            {
                string choosing = Console.ReadLine();

                if (choosing == "1")
                {
                    message.ShowListFriends(user, HaveFriends(user));
                    choosingID = HaveFriends(user) ? EnterFriendId(user, message) : EnterIdAllUsers(user, message);
                    break;
                }
                else if (choosing == "2")
                {
                    message.ShowListUsers(user);
                    choosingID = EnterIdAllUsers(user, message);
                    break;
                }
                else
                {
                    message.RepeatChooseOneOrTwo();
                }               
            }

            return choosingID;
        }

        public static void StartChatWithUser(User user, int id, IMessage message)
        {
            message.StartChatWithUser(id);
            

            for (; ; )
            {
                var checkFile = File.Exists(Path.GetPathChatFile(user, id));

                if (!checkFile)
                {
                    Chat.SetMessage(user, id, message);
                }
                else if(SetOrDeleteMessage(user, id, message))
                {
                    Chat.SetMessage(user, id, message);
                }
                else
                {
                    Chat.DeleteMessage(user, id, message);
                }                

                if(!ElseSetOrNot(message))
                {
                    break;
                }
            }           
        }

        private static bool SetOrDeleteMessage(User user, int id, IMessage message)
        {
            Show.ShowChat(user, id);
            message.SetOrDeleteMessage();

            for (; ; )
            {
                string choosing = Console.ReadLine();

                if (choosing == "1")
                {
                    return true;
                }
                else if (choosing == "2")
                {
                    return false;
                }
                else
                {
                    message.RepeatChooseOneOrTwo();
                }
            }
        }

        private static bool ElseSetOrNot(IMessage message)
        {
            message.ElseSetOrNot();

            for (; ; )
            {
                string choosing = Console.ReadLine();

                if (choosing == "1")
                {
                    return true;
                }
                else if (choosing == "2")
                {
                    Console.Clear();
                    return false;
                }
                else
                {
                    message.RepeatChooseOneOrTwo();
                }
            }
        }

        public static bool ContinueOrExit(IMessage message)
        {
            message.ContinueOrExit();

            for (; ; )
            {
                string choosing = Console.ReadLine();

                if (choosing == "1")
                {
                    Console.Clear();
                    return true;
                }
                else if (choosing == "2")
                {
                    return false;
                }
                else
                {
                    message.RepeatChooseOneOrTwo();
                }
            }
        }

        private static bool HaveFriends(User user)
        {
            if (user.friendsId.Count != 0)
            {
                return true;
            }

            return false;
        }

        private static int EnterIdAllUsers(User user, IMessage message)
        {
            for (; ; )
            {
                var tryConvert = int.TryParse((Console.ReadLine()), out int id);

                if (tryConvert && UserData.CheckIdAllUsersWithoutUser(user, id))
                {
                    return id;
                }

                message.RepeatSelectId();
            }
        }

        private static int EnterFriendId(User user, IMessage message)
        {
            for(; ;)
            {
                var tryConvert = int.TryParse((Console.ReadLine()), out int friendId);
                
                if(tryConvert && UserOperation.CheckIdFriend(user, friendId))
                {
                    return friendId;
                }

                message.RepeatSelectId();
            }
        }

        private static int EnterRequestFriendId(User user, IMessage message)
        {
            for (; ; )
            {
                var tryConvert = int.TryParse((Console.ReadLine()), out int requestToFriendId);

                if (tryConvert && UserOperation.CheckIdRequestFriend(user, requestToFriendId))
                {
                    return requestToFriendId;
                }

                message.RepeatSelectId();
            }
        }

        private static void AcceptOrDeleteFriend(User user, int friendId, IMessage message)
        {
            message.AcceptOrDeleteFriend(user, friendId);

            for(; ; )
            {
                string choosing = Console.ReadLine();

                if (choosing == "1")
                {
                    UserOperation.AcceptRequest(user, friendId, message);
                    break;
                }
                else if (choosing == "2")
                {
                    UserOperation.RejectRequest(user, friendId, message);
                    break;
                }
                else
                {
                    message.RepeatChooseOneOrTwo();
                }
            }
        }
    }
}
