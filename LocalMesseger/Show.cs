﻿using System;
using System.Collections;

namespace LocalMesseger
{
    public class Show
    {
        public static void ShowChat(User user, int friendId)
        {
            Console.Clear();
            string chat = FileOperation.LoadTextInformation(Path.GetPathChatFile(user, friendId), UserData.GetKey(user, friendId));
            Console.WriteLine(chat + "\n");
        }

        public static void ShowListFriends(User user)
        {
            for (int i = 0; i < UserData.GetListFriendsOrRequestFriendsForShow(user.friendsId).Count; i++)
            {
                Console.WriteLine(UserData.GetListFriendsOrRequestFriendsForShow(user.friendsId)[i]);
            }
        }

        public static void ShowChatForDelete(User user, int friendId)
        {
            Console.Clear();
            string chat = FileOperation.LoadTextInformation(Path.GetPathChatFile(user, friendId), UserData.GetKey(user, friendId));
            var chatForDelete = Chat.ChangeChatToChatForDelete(chat.Split('\n'));
            ShowArray(chatForDelete);
        }

        public static void ShowListUsers(User user)
        {
            ArrayList listUsers = UserData.GetListNameUsersWithoutUser(user);
            for (int i = 0; i < listUsers.Count; i++)
            {
                Console.WriteLine(listUsers[i]);
            }
        }

        public static void ShowListFriendsIdRequest(User user)
        {
            for (int i = 0; i < UserData.GetListFriendsOrRequestFriendsForShow(user.friendsIdRequest).Count; i++)
            {
                Console.WriteLine(UserData.GetListFriendsOrRequestFriendsForShow(user.friendsIdRequest)[i]);
            }
        }

        private static void ShowArray(string[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine(arr[i]);

                if (i == arr.Length - 1)
                {
                    Console.WriteLine();
                }
            }
        }
    }
}
