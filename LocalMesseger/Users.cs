﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace LocalMesseger
{
    public class Users
    {       
        public static int lastId = GetLastId();
        public static ArrayList listNameUsers = GetListNameUsers();
        public static ArrayList listLoginUsers = GetListLoginUsers();

        private static int GetLastId()
        {
            if(File.Exists(Path.ToLastIdFile) == false)
            {
                int lastId = 0;
                CreateDataUsersFolder();
                CreateLastIdFile(lastId);
            }

            string id = FileOperation.LoadTextInformation(Path.ToLastIdFile);

            return Convert.ToInt32(id);
        }

        private static ArrayList GetListNameUsers()
        {
            if (File.Exists(Path.ToUsersNameFile) == false)
            {
                using (FileStream fstream = new FileStream(Path.ToUsersNameFile, FileMode.Create)) { }
            }

            string allUsers = FileOperation.LoadTextInformation(Path.ToUsersNameFile);
            var arrayAllUsers = allUsers.Split('\n');
            ArrayList listUsers = new ArrayList();

            for (int i = 0; i < arrayAllUsers.Length; i++)
            {
                listUsers.Add(arrayAllUsers[i]);
            }

            return listUsers;
        }

        private static ArrayList GetListLoginUsers()
        {
            if (File.Exists(Path.ToUsersLoginFile) == false)
            {
                using (FileStream fstream = new FileStream(Path.ToUsersLoginFile, FileMode.Create)) { }
            }

            string allUsers = FileOperation.LoadTextInformation(Path.ToUsersLoginFile);
            var arrayAllUsers = allUsers.Split('\n');
            ArrayList listUsers = new ArrayList();

            for (int i = 0; i < arrayAllUsers.Length; i++)
            {
                listUsers.Add(arrayAllUsers[i]);
            }

            return listUsers;
        }

        private static void CreateDataUsersFolder()
        {
            DirectoryInfo dirInfo = new DirectoryInfo(Path.ToFolderDataUsers);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }
        }

        public static void CreateLastIdFile(int lastId)
        {
            var text = $"{lastId}";

            using (FileStream fstream = new FileStream(Path.ToLastIdFile, FileMode.Create))
            {
                byte[] array = System.Text.Encoding.Default.GetBytes(text);
                fstream.Write(array, 0, array.Length);
            }
        }

        private static ArrayList GetListNameUsersWithoutUser(User user)
        {
            ArrayList listUsersWithourUser = GetListNameUsers();
            listUsersWithourUser.RemoveAt(user.id - 1);
            return listUsersWithourUser;
        }

        public static void ShowListUsers(User user)
        {
            ArrayList listUsers = GetListNameUsersWithoutUser(user);
            for (int i = 0; i < listUsers.Count; i++)
            {
                Console.WriteLine(listUsers[i]);
            }
        }

        public static string GetNameUser(int id)
        {
            int additionalCountForSubring = 3;
            string nameUser = listNameUsers[id - 1].ToString();
            int numberDigits = GetNumberDigits(id);
            nameUser = nameUser.Substring(numberDigits + additionalCountForSubring);

            return nameUser;
        }

        public static string GetLoginUser(int id)
        {
            int additionalCountForSubring = 3;
            string loginUser = listLoginUsers[id - 1].ToString();
            int numberDigits = GetNumberDigits(id);
            loginUser = loginUser.Substring(numberDigits + additionalCountForSubring);

            return loginUser;
        }

        private static int GetNumberDigits(int id)
        {
            var stringId = $"{id}";

            return stringId.Length;
        }

        public static void ShowListFriends(User user)
        {
            for (int i = 0; i < user.friendsId.Count; i++)
            {
                Console.WriteLine(listNameUsers[Convert.ToInt32(user.friendsId[i])- 1]);
            }
        }

        public static bool CheckIdAllUsersWithoutUser(User user, int id)
        {
            if(id > lastId || id == user.id || id <= 0)
            {
                return false;
            }

            return true;
        }
    }
}
