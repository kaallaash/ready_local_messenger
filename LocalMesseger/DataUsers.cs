﻿using System;
using System.Collections;
using System.IO;

namespace LocalMesseger
{
    public class DataUsers
    {
        public static ArrayList listNameUsers()
        {
            return GetListNameOrLoginUsers(Path.ToUsersNameFile);
        }

        public static ArrayList listLoginUsers()
        {
            return GetListNameOrLoginUsers(Path.ToUsersLoginFile);
        }

        public static int GetLastId()
        {
            string id = FileOperation.LoadTextInformation(Path.ToLastIdFile, Encryption.Key);

            return Convert.ToInt32(id);
        }

        private static ArrayList GetListNameOrLoginUsers(string path)
        {
            if (!File.Exists(path))
            {
                using (FileStream fstream = new FileStream(path, FileMode.Create)) { }
            }

            string allUsers = FileOperation.LoadTextInformation(path, Encryption.Key);
            var arrayAllUsers = allUsers.Split('\n');
            ArrayList listUsers = new ArrayList();

            for (int i = 0; i < arrayAllUsers.Length; i++)
            {
                listUsers.Add(arrayAllUsers[i]);
            }

            return listUsers;
        }

        private static ArrayList GetListNameUsersWithoutUser(User user)
        {
            ArrayList listUsersWithourUser = GetListNameOrLoginUsers(Path.ToUsersNameFile);
            listUsersWithourUser.RemoveAt(user.id - 1);
            return listUsersWithourUser;
        }

        public static void ShowListUsers(User user)
        {
            ArrayList listUsers = GetListNameUsersWithoutUser(user);
            for (int i = 0; i < listUsers.Count; i++)
            {
                Console.WriteLine(listUsers[i]);
            }
        }

        public static string GetNameUser(int id)
        {
            int additionalCountForSubring = 3;
            string nameUser = listNameUsers()[id - 1].ToString();
            int numberDigits = OtherOperation.GetNumberDigits(id);
            nameUser = nameUser.Substring(numberDigits + additionalCountForSubring);

            return nameUser;
        }

        public static string GetLoginUser(int id)
        {
            int additionalCountForSubring = 3;
            string loginUser = listLoginUsers()[id - 1].ToString();
            int numberDigits = OtherOperation.GetNumberDigits(id);
            loginUser = loginUser.Substring(numberDigits + additionalCountForSubring);

            return loginUser;
        }
        
        public static void ShowListFriends(User user)
        {
            for (int i = 0; i < GetListFriendsOrRequestFriendsForShow(user.friendsId).Count; i++)
            {
                Console.WriteLine(GetListFriendsOrRequestFriendsForShow(user.friendsId)[i]);
            }
        }

        public static void ShowListFriendsIdRequest(User user)
        {
            for (int i = 0; i < GetListFriendsOrRequestFriendsForShow(user.friendsIdRequest).Count; i++)
            {
                Console.WriteLine(GetListFriendsOrRequestFriendsForShow(user.friendsIdRequest)[i]);
            }
        }

        public static bool CheckIdAllUsersWithoutUser(User user, int id)
        {
            if(id > GetLastId() || id == user.id || id <= 0)
            {
                return false;
            }

            return true;
        }

        private static ArrayList GetListFriendsOrRequestFriendsForShow(ArrayList listId)
        {
            ArrayList listForShow = new ArrayList();
            
            for (int i = 0; i < listId.Count; i++)
            {
                string id = OtherOperation.FindIdFromString(Convert.ToString(listId[i]));
                string name = GetNameUser(Convert.ToInt32(id));
                listForShow.Add($"{id} - {name}");
            }

            return listForShow;
        } 
        
        public static int GetKey(User user, int idSecondUser)
        {
            string[] pathToFindKey = new string[4];
            pathToFindKey[0] = Path.GetPathUserFriendsFile(user.login);
            pathToFindKey[1] = Path.GetPathUserFriendsFile(DataUsers.GetLoginUser(idSecondUser));
            pathToFindKey[2] = Path.GetPathUserFriendsRequestFile(user.login);
            pathToFindKey[3] = Path.GetPathUserFriendsRequestFile(DataUsers.GetLoginUser(idSecondUser));
            string key = "";

            for (int i = 0; i < pathToFindKey.Length; i++)
            {
                key = TryToFindKeyFromFile(pathToFindKey[i], user.id, idSecondUser);
                
                if (key != "")
                {
                    break;
                }
            }

            if (key == "")
            {
                key = "0";
            }

            return Convert.ToInt32(key);
        }

        private static string TryToFindKeyFromFile(string path, int id1, int id2)
        {
            string key = "";

            if(!File.Exists(path))
            {
                return key;
            }

            string searchString1 = $"{id1} - ";
            string searchString2 = $"{id2} - ";
            var textFromFile = FileOperation.LoadTextInformation(path, Encryption.Key);
            var textArray = textFromFile.Split('\n');
            

            for (int i = 0; i < textArray.Length; i++)
            {
                int index1 = textArray[i].IndexOf(searchString1);
                int index2 = textArray[i].IndexOf(searchString2);

                if (index1 != -1)
                {
                    key = textArray[i].Substring(searchString1.Length);
                    break;
                }
                if (index2 != -1)
                {
                    key = textArray[i].Substring(searchString2.Length);
                    break;
                }
            }

            return key;
        }
    }
}
