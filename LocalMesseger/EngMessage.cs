﻿using System;
using System.Threading;

namespace LocalMesseger
{
    class EngMessage : IMessage
    {
        public void ShowLanguage()
        {
            Console.WriteLine("You have chosen English");
        }

        public void StartProgram()
        {
            Console.WriteLine("1 - Log in\n2 - Sign up:");
        }

        public void RepeatChooseOneOrTwo()
        {
            Console.WriteLine("Make the correct input, key \"1\" or key \" 2\"");
        }

        public void SetLogin()
        {
            Console.WriteLine("Enter your username and password");
            Console.Write("Username: ");
        }

        public void SetPassword()
        {
            Console.Write("Password: ");
        }

        public void SetPasswordOneMoreTime()
        {
            Console.Write("enter the password again: ");
        }

        public void WrongPassword()
        {
            Console.WriteLine("The passwords don't match. Try again.");
            Thread.Sleep(1200);
            Console.Clear();
        }

        public void WrongLog()
        {
            Console.Clear();
            Console.WriteLine("This username is already in the system.\nCome up with a different username or log in.");
        }

        public void WrongPasLength()
        {
            Console.Clear();
            Console.Write("The password must be more than 3 characters long\nTry again: ");
        }

        public void WrongLogLength()
        {
            Console.Clear();
            Console.Write("The username must be more than 3 characters long\nTry again: ");
        }

        public void WrongLogPasChars()
        {
            Console.Clear();
            Console.Write("You can't use symbols: <, >, ?, /, \\, |, \",\", *, :, \" or a space\nTry again: ");
        }

        public void WrongLogOrPas()
        {
            Console.Clear();
            Console.WriteLine("Incorrect username or password. Try again.");
        }

        public void HelloUser(User user)
        {
            Console.Clear();
            Console.WriteLine($"Hello {user.name}!!!");
        }

        public void HelloNewUser(User user)
        {
            Console.Clear();
            Console.WriteLine($"Hello {user.name}!!!\nYou have successfully registered!\nYour ID: {user.id}");
        }

        public void SetName()
        {
            Console.Write($"Enter your name: ");
        }

        public void RepeatSetName()
        {
            Console.Write($"The name must be more than two characters and not consist of only spaces.\n Try again: ");
        }

        public void StartChatWithUser(int id)
        {
            Console.Clear();
            Console.WriteLine($"You have entered the chat with {UserData.GetNameUser(id)}");
        }

        public void ChooseShowRequestOrContinue(User user)
        {
            if (user.friendsIdRequest.Count > 1)
            {
                Console.WriteLine($"You have new friend requests\n" +
                "1 - Show requests to friends\n2 - Continue without viewing:");
            }
            else
            {
                Console.WriteLine($"You have a new friend request\n" +
                "1 - Show the request to friends\n2 - Continue without viewing:");
            }
        }

        public void ChooseChat()
        {
            Console.WriteLine("Who do you want to start a dialogue with?\n" +
                "1-Select from friends list \n2-Select from all users list:");
        }

        public void ShowListFriends(User user, bool haveFriend)
        {
            if (haveFriend)
            {
                Console.WriteLine("Select an ID from your friends list:");
                Show.ShowListFriends(user);
            }
            else
            {
                Console.WriteLine("Unfortunately you don't have any friends yet" +
                    " so choose an ID from the list of all users");
                Show.ShowListUsers(user);
            }

            Console.Write("ID: ");
        }

        public void ShowListFriendsRequest(User user)
        {
            Console.WriteLine("Select an ID from this list:");
            Show.ShowListFriendsIdRequest(user);
            Console.Write("ID: ");
        }

        public void ShowListUsers(User user)
        {
            Console.WriteLine("Select an ID from the list of all users");
            Show.ShowListUsers(user);
            Console.Write("ID: ");
        }

        public void RepeatSelectId()
        {
            Console.Write("Select an existing ID from this list\nID: ");
        }

        public void SetOrDeleteMessage()
        {
            Console.Write("What do you want to do?" +
                "\n1 - Write a message\n2 - Delete a message\nMake a choice: ");
        }

        public void SetMessage()
        {
            Console.Write("Write a message: ");
        }

        public void NoDialog()
        {
            Console.WriteLine("You still have no correspondence with this user");
        }

        public void SetNumberDeletedMessage()
        {
            Console.Write("Enter the number of the message to delete. №: ");
        }

        public void RepeatSetNumberDeletedMessage(int countMessage)
        {
            Console.Write($"Enter the correct number of the message to delete, from 1 to {countMessage}. №: ");
        }

        public void ElseSetOrNot()
        {
            Console.Write("Stay in the dialog with this user?\n1 - Yes\n2 - No\nMake a choice: ");
        }

        public void SetKey()
        {
            Console.Write("Write a key (from 1 to 10): ");
        }

        public void RepeatSetKey()
        {
            Console.Write($"The key must be in the range from 1 to 10.\nTry again: ");
        }

        public void AcceptOrDeleteFriend(User user, int idFriend)
        {
            Console.Clear();
            Console.Write($"You have selected the user {UserData.GetNameUser(idFriend)}\n" +
                $"{UserData.GetNameUser(idFriend)} offers you the dialog encryption key: " +
                $"{UserData.GetKey(user, idFriend)}\n" +
                $"1 - Accept the application as a friend\n2 - Reject\nMake a selection: ");
        }

        public void AcceptRequest(int id)
        {
            Console.Clear();
            Console.WriteLine($"You have accepted the application as a friend." +
                $"\n{UserData.GetNameUser(id)} is added to your friends list");
        }

        public void RejectRequest(int id)
        {
            Console.Clear();
            Console.WriteLine($"You rejected a friend request from {UserData.GetNameUser(id)}");
        }

        public void ContinueOrExit()
        {
            Console.Write("Stay in the program?\n1 - Yes\n2 - No (exit)\nMake a choice: ");
        }

        public void YouOnlyUser()
        {
            Console.WriteLine("You are the only user who has signed up for the program" +
                "\nso wait until someone else creates their profiles");
            Thread.Sleep(7500);
        }

        public void GoodBye()
        {
            Console.Clear();
            Console.WriteLine("GoodBye! =)");
            Thread.Sleep(2500);
        }
    }
}
