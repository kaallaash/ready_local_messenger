﻿namespace LocalMesseger
{
    public interface IMessage
    {
        void ShowLanguage();

        void RepeatChooseOneOrTwo();

        void StartProgram();

        void SetLogin();

        void SetPassword();

        void SetPasswordOneMoreTime();

        void WrongPassword();

        void WrongPasLength();

        void WrongLog();

        void WrongLogLength();

        void WrongLogPasChars();

        void WrongLogOrPas();

        void HelloUser(User user);

        void HelloNewUser(User user);

        void SetName();

        void RepeatSetName();

        void StartChatWithUser(int id);

        void ChooseShowRequestOrContinue(User user);

        void ChooseChat();

        void ShowListFriends(User user, bool haveFriend);

        void ShowListFriendsRequest(User user);

        void ShowListUsers(User user);

        void RepeatSelectId();

        void SetOrDeleteMessage();

        void SetMessage();

        void NoDialog();

        void SetNumberDeletedMessage();

        void RepeatSetNumberDeletedMessage(int countMessage);

        void ElseSetOrNot();

        void SetKey();

        void RepeatSetKey();

        void AcceptOrDeleteFriend(User user, int idFriend);

        void AcceptRequest(int id);

        void RejectRequest(int id);

        void ContinueOrExit();

        void YouOnlyUser();

        void GoodBye();
    }
}
