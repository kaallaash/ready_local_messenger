﻿using System;

namespace LocalMesseger
{
    class UserOperation
    {
        public static void SendRequestToFriend(User user, int friendId, int key)
        {
            string path = Path.GetPathUserFriendsRequestFile(UserData.GetLoginUser(friendId));
            string text = IdFriendWithKey(user.id, key);
            FileOperation.WriteToFile(text, path, Encryption.Key);
        }

        public static bool CheckIdFriend(User user, int friendId)
        {
            if (user.friendsId.Count == 0)
            {
                return false;
            }

            for (int i = 0; i < user.friendsId.Count; i++)
            {
                if (friendId == Convert.ToInt32(OtherOperation.FindIdFromString(Convert.ToString(user.friendsId[i]))))
                {
                    return true;
                }
            }

            return false;
        }

        public static bool CheckHaveRequestFriend(User user)
        {
            if (user.friendsIdRequest.Count > 0 && Convert.ToString(user.friendsIdRequest[0]).Length > 0)
            {
                return true;
            }

            return false;           
        }

        public static bool CheckIdRequestFriend(User user, int requestToFriendId)
        {
            for (int i = 0; i < user.friendsIdRequest.Count; i++)
            {
                if (requestToFriendId == Convert.ToInt32(OtherOperation.FindIdFromString(Convert.ToString(user.friendsIdRequest[i]))))
                {
                    return true;
                }
            }

            return false;
        }

        public static void AcceptRequest(User user, int requestToFriendId, IMessage message)
        {
            AddFriend(user, requestToFriendId, UserData.GetKey(user, requestToFriendId));
            DeleteRequest(user, requestToFriendId);
            message.AcceptRequest(requestToFriendId);
        }

        public static void RejectRequest(User user, int requestToFriendId, IMessage message)
        {
            DeleteRequest(user, requestToFriendId);
            message.RejectRequest(requestToFriendId);
        }

        private static void DeleteRequest(User user, int deletedFriendId)
        {
            string path = Path.GetPathUserFriendsRequestFile(user.login);
            string text = FileOperation.LoadTextInformation(path, Encryption.Key);            
            string deletedString = $"{deletedFriendId} - {UserData.GetKey(user, deletedFriendId)}";
            string newText = OtherOperation.DeletedLineFromString(text, deletedString);
            FileOperation.OverwriteToFile(newText, path, Encryption.Key);
            user.LoadData(user.login);
        }

        private static void AddFriend(User user, int friendId, int key)
        {
            user.friendsId.Add(IdFriendWithKey(friendId, key));
            FileOperation.WriteToFile(IdFriendWithKey(friendId, key), Path.GetPathUserFriendsFile(user.login), Encryption.Key);
            FileOperation.WriteToFile(IdFriendWithKey(user.id, key), Path.GetPathUserFriendsFile(UserData.GetLoginUser(friendId)), Encryption.Key);
        }

        private static string IdFriendWithKey(int friendId, int key)
        { 
            return $"{friendId} - {key}";
        }
    }
}
