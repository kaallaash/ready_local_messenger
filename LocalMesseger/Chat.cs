﻿using System;
using System.IO;

namespace LocalMesseger
{
    public class Chat
    {
        private static void CreateChatFile(User user, int friendId)
        {
            CreateChatFolder(user, friendId);
            using (FileStream fstream = new FileStream(Path.GetPathChatFile(user, friendId), FileMode.Create)) { }
        }

        private static void CreateChatFolder(User user, int friendId)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(Path.GetPathChatFolder(user, friendId));
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }
        }

        public static void SetMessage(User user, int friendId, IMessage message)
        {
            var checkFile = File.Exists(Path.GetPathChatFile(user, friendId));

            if (!checkFile)
            {
                CreateChatFile(user, friendId);
                int key = SetKey(message);
                UserOperation.SendRequestToFriend(user, friendId, key);
            }
            else
            {
                Show.ShowChat(user, friendId);
            }

            message.SetMessage();
            var input = Console.ReadLine();
            var textMessage = MessageForSend(input, user);

            FileOperation.WriteToFile(textMessage, Path.GetPathChatFile(user, friendId), UserData.GetKey(user, friendId));
            Show.ShowChat(user, friendId);
        }

        public static void DeleteMessage(User user, int friendId, IMessage message)
        {
            var checkFile = File.Exists(Path.GetPathChatFile(user, friendId));

            if (!checkFile)
            {
                message.NoDialog();
            }
            else
            {
                Show.ShowChatForDelete(user, friendId);
                int numberDeletedMessage = SetNumberDeletedMessage(user, friendId, message);
                FileOperation.OverwriteToFile(ChangedChatAfterDelete(user, friendId, numberDeletedMessage), Path.GetPathChatFile(user, friendId), UserData.GetKey(user, friendId));
                Show.ShowChat(user, friendId);                
            }
        }

        public static int SetKey(IMessage message)
        {
            message.SetKey();

            string input;
            int key;

            for (; ; )
            {
                input = Console.ReadLine();

                try
                {
                    var tryConvert = int.TryParse(input, out int intKey);

                    if (intKey > 0 && intKey < 101)
                    {
                        key = intKey;
                        break;
                    }
                }
                catch { }

                message.RepeatSetKey();
            }

            return key;
        }                

        private static string MessageForSend(string input, User user)
        {
            var dateTime = DateTime.Now;
            var time = $"{dateTime.ToString("d")} {dateTime.ToString("t")}";
            var name = user.name;
            string glueMessage = $"{name} {time}\n  {input}";
            return glueMessage;
        }

        private static string ChangedChatAfterDelete(User user, int friendId, int numberDeletedMessage)
        {
            string chat = FileOperation.LoadTextInformation(Path.GetPathChatFile(user, friendId), UserData.GetKey(user, friendId));
            string[] changedChat = ChangeDeletedMessege(chat.Split('\n'), user, numberDeletedMessage);
            return OtherOperation.ConvertToStringWithParagraph(changedChat);

        }

        private static string[] ChangeDeletedMessege(string[] chat, User user, int numberDeletedMessage)
        {
            chat[(numberDeletedMessage * 2) - 1] = ChangedDeletedMessage(user);
            return chat;
        }

        private static string ChangedDeletedMessage(User user)
        {
            var dateTime = DateTime.Now;
            var time = $"{dateTime.ToString("d")} {dateTime.ToString("t")}";
            return $"  Message was deleted by {user.name} {time}";
        }                

        public static string[] ChangeChatToChatForDelete(string[] chat)
        {
            var chatToChatForDelete = new string[chat.Length];
            int idMessage = 1;

            for (int i = 0; i < chatToChatForDelete.Length; i++)
            {
                if (i % 2 == 1)
                {
                    chatToChatForDelete[i] = $" №{idMessage} - {chat[i]}";
                    idMessage++;
                }
                else
                {
                    chatToChatForDelete[i] = chat[i];
                }
            }

            return chatToChatForDelete;
        }               

        private static int SetNumberDeletedMessage(User user, int friendId, IMessage message)
        {
            message.SetNumberDeletedMessage();
            int number;

            for (; ; )
            {
                var input = Console.ReadLine();

                try
                {
                    var tryConvert = int.TryParse(input, out number);

                    if (number > 0 && number <= GetCountMessages(user, friendId))
                    {
                        break;
                    }
                }
                catch { }

                message.RepeatSetNumberDeletedMessage(GetCountMessages(user, friendId));
            }

            return number;
        }

        private static int GetCountMessages(User user, int friendId)
        {
            string message = FileOperation.LoadTextInformation(Path.GetPathChatFile(user, friendId), UserData.GetKey(user, friendId));
            return message.Split('\n').Length / 2;
        }
    }
}
