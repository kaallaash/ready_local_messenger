﻿namespace LocalMesseger
{
    class Path
    {
        public const string ToFolderUsers = @"Users";
        public const string ToFolderDataUsers = @"DataUsers";
        public const string ToFolderUsersChat = @"UsersChat";
        public const string ToLastIdFile = @"DataUsers\lastId.txt";
        public const string ToUsersNameFile = @"DataUsers\usersName.txt";
        public const string ToUsersLoginFile = @"DataUsers\usersLogin.txt";

        public static string GetPathUserFolder(string userLogin)
        {
            return $@"{ToFolderUsers}\{userLogin}";
        }

        public static string GetPathUserDataFile(string userLogin)
        {
            return $@"{GetPathUserFolder(userLogin)}\data.txt";
        }

        public static string GetPathUserFriendsFile(string userLogin)
        {
            return $@"{GetPathUserFolder(userLogin)}\friends.txt";
        }

        public static string GetPathUserFriendsRequestFile(string userLogin)
        {
            return $@"{GetPathUserFolder(userLogin)}\friendsRequest.txt";
        }

        public static string GetPathChatFolder(User user, int friendId)
        {
            return $@"{Path.ToFolderUsersChat}\{GetNameFolderChat(user, friendId)}";
        }

        public static string GetPathChatFile(User user, int friendId)
        {
            return $@"{Path.ToFolderUsersChat}\{GetNameFolderChat(user, friendId)}\chat.txt";
        }

        private static string GetNameFolderChat(User user, int friendId)
        {
            if (user.id > friendId)
            {
                return $"{friendId}-{user.id}";
            }
            else
            {
                return $"{user.id}-{friendId}";
            }
        }
    }
}
