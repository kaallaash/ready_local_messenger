﻿using System;
using System.IO;

namespace LocalMesseger
{
    class FileOperation
    {
        public static string LoadTextInformation(string path, int key)
        {
            using (FileStream fstream = File.OpenRead(path))
            {
                byte[] array = new byte[fstream.Length];
                fstream.Read(array, 0, array.Length);
                return Encryption.Decrypt(System.Text.Encoding.Default.GetString(array), key);
            }
        }

        public static void CreateNeedFiles()
        {
            if (!File.Exists(Path.ToLastIdFile))
            {
                int lastId = 0;
                CreateDataUsersFolder();
                CreateLastIdFile(lastId);
            }
        }

        public static void ChangeLastIdAfterReggistration(User user)
        {
            OverwriteToFile(Convert.ToString(user.id), Path.ToLastIdFile, Encryption.Key);
        }

        public static void ChangeUsersNameFileAfterReggistration(User user)
        {
            var text = LastUserName(user);
            WriteToFile(text, Path.ToUsersNameFile, Encryption.Key);
        }

        public static void ChangeUsersLoginFileAfterReggistration(User user)
        {
            var text = LastUserLogin(user);
            WriteToFile(text, Path.ToUsersLoginFile, Encryption.Key);
        }

        private static string LastUserName(User user)
        {
            return $"{user.id} - {user.name}";
        }

        private static string LastUserLogin(User user)
        {
            return $"{user.id} - {user.login}";
        }

        public static void OverwriteToFile(string text, string pathFile, int key)
        {
            text = Encryption.Encrypt(text, key);

            using (FileStream fstream = new FileStream(pathFile, FileMode.Create))
            {
                byte[] array = System.Text.Encoding.Default.GetBytes(text);
                fstream.Write(array, 0, array.Length);
            }
        }

        public static void WriteToFile(string text, string pathFile, int key)
        {
            string space = " ";

            if (File.Exists(pathFile) && LoadTextInformation(pathFile, key).Length > 0)
            {
                text = "\n" + text;
                text = space + Encryption.Encrypt(text, key);
            }
            else
            {
                text = Encryption.Encrypt(text, key);
            }            

            using (FileStream fstream = new FileStream(pathFile, FileMode.Append))
            {
                byte[] array = System.Text.Encoding.Default.GetBytes(text);
                fstream.Write(array, 0, array.Length);
            }
        }

        private static void CreateDataUsersFolder()
        {
            DirectoryInfo dirInfo = new DirectoryInfo(Path.ToFolderDataUsers);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }
        }

        private static void CreateLastIdFile(int lastId)
        {
            OverwriteToFile(Convert.ToString(lastId), Path.ToLastIdFile, Encryption.Key);
        }

        public static void CreateUserFolder(string login)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(Path.GetPathUserFolder(login));
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }
        }

        public static void CreateUserDataFile(string name, int id, string login, string password)
        {
            var text = $"{name}\n{id}\n{login}\n{password}";
            text = Encryption.Encrypt(text, Encryption.Key);

            using (FileStream fstream = new FileStream(Path.GetPathUserDataFile(login), FileMode.Create))
            {
                byte[] array = System.Text.Encoding.Default.GetBytes(text);
                fstream.Write(array, 0, array.Length);
            }
        }
    }
}
