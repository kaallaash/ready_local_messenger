﻿using System;
using System.IO;
using System.Threading;

namespace LocalMesseger
{
    class Settings
    {
        public static IMessage GetMessagesLanguage()
        {
            Console.WriteLine("Выберите Язык: \n1 - Русский \n2 - English");
            IMessage message;
            for (; ; )
            {
                string language = Console.ReadLine();

                if (language == "1")
                {
                    message = new RuMessage();
                    break;
                }
                else if (language == "2")
                {
                    message = new EngMessage();
                    break;
                }
                else
                {
                    Console.WriteLine("Выберите правильно язык, клавиша \"1\" или клавишу \"2\"");
                }
            }

            Console.Clear();
            message.ShowLanguage();
            Thread.Sleep(900);
            return message;
        }

        public static User GetUser(IMessage message)
        {
            for (; ; )
            {
                message.StartProgram();
                string choosing = Console.ReadLine();

                if (choosing == "1")
                {
                    var login = EnterLogin(message);
                    var password = EnterPassword(message);
                    
                    if(CheckLogAndPass(login, password))
                    {
                        User user = new User();
                        user.LoadData(login);
                        message.HelloUser(user);
                        return user;
                    }
                    else
                    {
                        message.WrongLogOrPas();
                    }
                }
                else if (choosing == "2")
                {
                    var login = EnterLogin(message);

                    if (!CheckLogin(login))
                    {
                        message.WrongLog();
                        continue;
                    }

                    var password = EnterPassword(message);
                    message.SetPasswordOneMoreTime();
                    string repeatPassword = Console.ReadLine();
                    
                    if (password != repeatPassword)
                    {
                        message.WrongPassword();
                    }
                    else
                    {
                        User user = new User();                        
                        string name = EnterName(message);
                        int id = Convert.ToInt32(FileOperation.LoadTextInformation(Path.ToLastIdFile, Encryption.Key)) + 1;
                        user.CreateData(name, id, login, password);
                        FileOperation.ChangeLastIdAfterReggistration(user);
                        FileOperation.ChangeUsersNameFileAfterReggistration(user);
                        FileOperation.ChangeUsersLoginFileAfterReggistration(user);
                        message.HelloNewUser(user);
                        return user;
                    }

                    continue;
                }
                else
                {
                    message.RepeatChooseOneOrTwo();
                }
            }
        }

        private static bool CheckLogAndPass(string login, string password)
        {
            try
            {
                User user = new User();
                user.LoadData(login);
                return user.CheckPassword(password);
            }
            catch
            {
                return false;
            }
        }

        private static bool CheckLogin(string login)
        {
            string[] users = GetNameUsers(Path.ToFolderUsers);

            for (int i = 0; i < users.Length; i++)
            {
                if (login == users[i])
                {
                    return false;
                }
            }

            return true;
        }

        private static string EnterName(IMessage message)
        {
            message.SetName();
            
            for (; ; )
            {
                string name = Console.ReadLine();
                if (CheckName(name))
                {
                    return name;
                }

                message.RepeatSetName();
            }
        }

        private static bool CheckName(string name)
        {
            if (CheckNameForLength(name) && CheckNameForOnlySpace(name))
            {
                return true;
            }

            return false;
        }

        private static bool CheckNameForLength(string name)
        {
            if (name.Length > 2)
            {
                return true;
            }

            return false;
        }

        private static bool CheckNameForOnlySpace(string name)
        {
            for (int i = 0; i < name.Length; i++)
            {
                if (name[i] != ' ')
                {
                    return true;
                }
            }

            return false;
        }

        private static string[] GetNameUsers(string PathToFolder)
        {
            string[] nameUsers;

            if (Directory.Exists(PathToFolder))
            {
                string[] allfolders = Directory.GetDirectories(PathToFolder);
                nameUsers = new string[allfolders.Length];

                int i = 0;
                foreach (string folder in allfolders)
                {
                    nameUsers[i] = folder.Substring(PathToFolder.Length + 1, folder.Length - PathToFolder.Length - 1);
                    i++;
                }
            }
            else
            {
                nameUsers = new string[0];
            }

            return nameUsers;
        }

        private static string EnterLogin(IMessage message)
        {
            message.SetLogin();
            string login;
            for (; ; )
            {
                login = Console.ReadLine();

                if (login.Length < 3)
                {
                    message.WrongLogLength();
                }
                else if (CheckLoginForBanChars(login))
                {
                    message.WrongLogPasChars();
                }
                else
                {
                    login = login.ToLower();
                    break;
                }
            }
            
            return login;
        }

        private static string EnterPassword(IMessage message)
        {
            message.SetPassword();
            string password;
            for (; ; )
            {
                password = Console.ReadLine();

                if (password.Length < 3)
                {
                    message.WrongPasLength();
                }
                else if (CheckLoginForBanChars(password))
                {
                    message.WrongLogPasChars();
                }
                else
                {
                    break;
                }
            }

            return password;
        }

        private static bool CheckLoginForBanChars(string login)
        {
            char[] banChars = { '<', '>', '?', '/', '\\', '|', '*', ':', '"', ' ', ',' };
            for (int i = 0; i < banChars.Length; i++)
            {
                int indexOfChar = login.IndexOf(banChars[i]);
                if (indexOfChar > -1)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
