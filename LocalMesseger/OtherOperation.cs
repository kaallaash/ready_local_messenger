﻿namespace LocalMesseger
{
    class OtherOperation
    {
        public static string FindIdFromString(string str)
        {
            var arrayStr = str.Split(' ');

            return arrayStr[0];
        }

        public static int GetNumberDigits(int id)
        {
            var stringId = $"{id}";

            return stringId.Length;
        }

        public static string DeletedLineFromString(string str, string deletedLine)
        {
            var textArr = str.Split('\n');
            var newStr = "";

            for (int i = 0; i < textArr.Length; i++)
            {
                if (textArr[i] != deletedLine && i < textArr.Length - 1)
                {
                    newStr += textArr[i] + "\n";
                }
                else if (textArr[i] != deletedLine)
                {
                    newStr += textArr[i];
                }
            }

            return newStr;
        }

        public static string ConvertToStringWithParagraph(string[] arr)
        {
            string str = "";

            for (int i = 0; i < arr.Length; i++)
            {
                if (i != arr.Length - 1)
                {
                    str += arr[i] + '\n';
                }
                else
                {
                    str += arr[i];
                }                
            }

            return str;
        }
    }
}
