﻿using System;

namespace LocalMesseger
{
    public class Messenger
    {
        public static void StartMessenger()
        {
            Console.Title = "LocalMesseger";
            FileOperation.CreateNeedFiles();
            IMessage message = Settings.GetMessagesLanguage();
            User user = Settings.GetUser(message);            

            for (; ; )
            {
                if(UserData.GetLastId() == 1)
                {
                    message.YouOnlyUser();
                    break;
                }

                MessengerOperations.CheckIdRequestFriend(user, message);
                MessengerOperations.StartChat(user, message);

                if(!MessengerOperations.ContinueOrExit(message))
                {
                    break;
                }
            }

            message.GoodBye();
        }
    }
}
