﻿using System;
using System.Threading;

namespace LocalMesseger
{
    class RuMessage : IMessage
    {
        public void ShowLanguage()
        {
            Console.WriteLine("Вы выбрали русский язык");
        }

        public void RepeatChooseOneOrTwo()
        {
            Console.WriteLine("Сделайте корректный ввод, клавиша \"1\" или клавиша \"2\"");
        }

        public void StartProgram()
        {
            Console.WriteLine("1 - Войти в систему\n2 - Новый пользователь:");
        }

        public void SetLogin()
        {
            Console.WriteLine("Введите Логин и пароль");
            Console.Write("Логин: ");
        }

        public void SetPassword()
        {
            Console.Write("Пароль: ");
        }

        public void SetPasswordOneMoreTime()
        {
            Console.Write("Введите пароль ещё раз: ");
        }

        public void WrongPassword()
        {
            Console.WriteLine("Пароли не совпадают. Попробуйте заново.");
            Thread.Sleep(1200);
            Console.Clear();
        }

        public void WrongPasLength()
        {
            Console.Clear();
            Console.Write("Пароль должен быть более 3-ёх символов\nПопробуйте ещё раз: ");
        }

        public void WrongLog()
        {
            Console.Clear();
            Console.WriteLine("Такой логин уже есть в системе\nПридумайте другой логин или авторизуйтесь.");
        }

        public void WrongLogLength()
        {
            Console.Clear();
            Console.Write("Логин должен быть более 3-ёх символов\nПопробуйте ещё раз: ");
        }

        public void WrongLogPasChars()
        {
            Console.Clear();
            Console.Write("Нельзя использовать символы: <, >, ?, /, \\, |, \",\", *, :, \" или пробел\nПопробуйте ещё раз: ");
        }            

        public void WrongLogOrPas()
        {
            Console.Clear();
            Console.WriteLine("Неправильный логин или пароль. Попробуйте заново.");
        }
       
        public void HelloUser(User user)
        {
            Console.Clear();
            Console.WriteLine($"Привет {user.name}!!!");
        }

        public void HelloNewUser(User user)
        {
            Console.Clear();
            Console.WriteLine($"Привет {user.name}!!!\nВы успешно зарегистрировались!\nВаш ID: {user.id}");
        }

        public void SetOrDeleteMessage()
        {
            Console.Write("Что вы хотите сделать?" +
                "\n1 - Написать сообщение\n2 - Удалить сообщение\nСделайте выбор: ");
        }

        public void SetName()
        {
            Console.Write($"Введите своё имя: ");
        }

        public void RepeatSetName()
        {
            Console.Write($"Имя должно быть больше двух символов и не состоять из одних пробелов.\n Попробуйте ещё раз: ");
        }

        public void StartChatWithUser(int id)
        {
            Console.Clear();
            Console.WriteLine($"Вы зашли в чат с {UserData.GetNameUser(id)}");
        }

        public void ChooseShowRequestOrContinue(User user)
        {
            if (user.friendsIdRequest.Count > 1)
            {
                Console.WriteLine($"У вас есть новые заявки в друзья\n" +
                "1 - Показать заявки в друзья\n2 - Продолжить без просмотра:");
            }
            else
            {
                Console.WriteLine($"У вас есть новая заявка в друзья\n" +
                "1 - Показать заявку в друзья\n2 - Продолжить без простомотра:");
            }
        }

        public void ChooseChat()
        {
            Console.WriteLine("С кем вы хотите начать диалог?\n" +
                "1 - Выбрать из списка друзей\n2 - Выбрать из списка всех пользователей:");
        }

        public void ShowListFriends(User user, bool haveFriend)
        {
            if (haveFriend)
            {
                Console.WriteLine("Выберите ID из списка друзей:");
                Show.ShowListFriends(user);
            }
            else
            {
                Console.WriteLine("К сожалению у вас ещё нет друзей," +
                       " поэтому выберите ID из списка всех пользователей");
                Show.ShowListUsers(user);
            }
            
            Console.Write("ID: ");
        }

        public void ShowListFriendsRequest(User user)
        {
            Console.WriteLine("Выберите ID из данного списка:");
            Show.ShowListFriendsIdRequest(user);
            Console.Write("ID: ");
        }

        public void ShowListUsers(User user)
        {
            Console.WriteLine("Выберите ID из списка всех пользователей");
            Show.ShowListUsers(user);
            Console.Write("ID: ");
        }

        public void RepeatSelectId()
        {
            Console.Write("Выберите существующий ID из данного списка\nID: ");
        }

        public void SetMessage()
        {
            Console.Write("Введите сообщение: ");
        }

        public void NoDialog()
        {
            Console.WriteLine("У вас ещё нету переписки с этим пользователем");
        }

        public void SetNumberDeletedMessage()
        {
            Console.Write("Введите номер удаляемого сообщения. №: ");
        }

        public void RepeatSetNumberDeletedMessage(int countMessage)
        {
            Console.Write($"Введите корректный номер удаляемого сообщения, от 1 до {countMessage}. №: ");
        }

        public void ElseSetOrNot()
        {
            Console.Write("Остаться в диалоге с этим пользователем?\n1 - Да\n2 - Нет\nСделайте выбор: ");
        }

        public void SetKey()
        {
            Console.Write("Введите Ключ (от 1 до 100): ");
        }

        public void RepeatSetKey()
        {
            Console.Write($"Ключ должен быть в диапозоне от 1 до 100.\nПопробуйте ещё раз: ");
        }

        public void AcceptOrDeleteFriend(User user, int friendId)
        {
            Console.Clear();
            Console.Write($"Вы выбрали пользователя {UserData.GetNameUser(friendId)}\n" +
                $"{UserData.GetNameUser(friendId)} предлагает вам ключ шифрования " +
                $"диалога: {UserData.GetKey(user, friendId)}\n" +
                $"1 - Принять заявку в друзья\n2 - Отклонить\nСделайте выбор: ");
        }

        public void AcceptRequest(int id)
        {
            Console.Clear();
            Console.WriteLine($"Вы приняли заявку в друзья.\n{UserData.GetNameUser(id)} добавлен в список ваших друзей");
        }

        public void RejectRequest(int id)
        {
            Console.Clear();
            Console.WriteLine($"Вы отлонили заявку в друзья от {UserData.GetNameUser(id)}");
        }

        public void ContinueOrExit()
        {
            Console.Write("Остаться в программе?\n1 - Да\n2 - Нет (Выйти)\nСделайте выбор: ");
        }

        public void YouOnlyUser()
        {
            Console.WriteLine("Вы единственный пользователь, который зарегистрировался в программе" +
                "\nпоэтому дождитесь пока кто-нибудь ещё создаст свои профили");
            Thread.Sleep(6500);
        }

        public void GoodBye()
        {
            Console.Clear();
            Console.WriteLine("До свидания! =)");
            Thread.Sleep(2500);
        }
    }
}
